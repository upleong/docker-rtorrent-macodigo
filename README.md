# Docker image macodigo/docker-rtorrent-macodigo

rTorrent + screen

sh into container to control with native rTorrent

copy file-to-be-download.torrent on host machine folder to trigger download

(optional) deployed with Kubernetes or Docker Compose

local folder (rtorrent/ in current directory) on host machine is mounted by container
creaet with command `make prepare`

### Docker Hub
Avaialble at, https://hub.docker.com/r/macodigo/docker-rtorrent-macodigo/


# build it
```
    make prepare
    make build
```

## local directories are mounted
putting .rtorrent to local directory (rtorrent/watch/{active,load}/) to active or load a download session
downloaded files in rtorrent/download

## rtorrent UI via screen
running through screen, then we can attach via screen instead of docker
```
    make exec-sh
    or
    docker exec -it rtorrent-experiment sh

    screen -x
    or
    screen -dr
    or
    screen -r
```
ctrl-a d to detach again

# workflow for downloading
start the rtorrent in docker
load or start with download.torrent according to the watch directory

```
    make run
    cp download.torrent into rtorrent/watch/start
```
sh into container to monitor progress
`make exec-sh`
detach and exit monitor, let rtorrent continues download
```
    ctrl-a d
```
quit rtorrent, then stop container
```
    ctrl-q to exit rtorrent
    make stop
```


# (option ii) build and run with "docker compose"
```
    make prepare
    make docker-build
    make docker-up
    make docker-sh
    make docker-down
```


# (option iii) Kubernetes deploy

## workflow with kubernetes locally with k3d
create cluster with local storage mount
deploy with rtorrent
load or start with file.torrent according to the watch directory (rtorrent/watch/{load,start})
```
    k3d cluster create rtorrent-exp -v $(PWD)/rtorrent:/mnt/rtorrent-exp@server[0]

    kubectl apply -f deploy
```

sh into container to monitor progress
```
	kubectl exec -it $(shell kubectl get pod -l app=${CONTAINER_NAME} -o jsonpath="{.items[0].metadata.name}") -- sh
```

copy file on host file system to trigger download in container
```
    cp file.torrent into rtorrent/watch/load
```

useful check status commands
```
    kubectl describe pod
    kubectl describe pv
    kubectl describe pvc
```

restart rtorrent by deleting pod
```
    kubectl delete pod $(shell kubectl get pod -l app=${CONTAINER_NAME} -o jsonpath="{.items[0].metadata.name}")
```

# reference project
https://github.com/rakshasa/rtorrent/wiki/User-Guide

# quick reference card
https://blbl.dev/rtorrent_ref.pdf
