######## Alpine Base #########
FROM alpine:latest as basement
MAINTAINER UP Leong <pio.leong@gmail.com>

## rtorrent ##
##
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
RUN apk add -q --progress --update --no-cache rtorrent screen
##

##
ARG APPDIR=/rtorrent
RUN mkdir -p ${APPDIR}/config.d
RUN mkdir -p /mnt/${APPDIR}/.session
RUN mkdir -p /mnt/${APPDIR}/download
RUN mkdir -p /mnt/${APPDIR}/watch
#
COPY files/config.d/ ${APPDIR}/config.d/
COPY files/.rtorrent.rc /root/
#
COPY files/.screenrc /root/
#

CMD ["screen", "rtorrent"]
