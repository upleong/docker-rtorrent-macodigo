TAGNAME=macodigo/docker-rtorrent-macodigo
CONTAINER_NAME=rtorrent-experiment
K3D_CLUSTER_NAME=rtorrent-exp

.PHONY: all

prepare:
	mkdir -p {rtorrent/.session,rtorrent/download,rtorrent/session}

all:
	@echo "rtorrent in docker"

build:
	docker build -t ${TAGNAME} -f Dockerfile .

push:
	docker push ${TAGNAME}

# for directly running with docker
run:
	docker run -dt --rm -p 50000:50000 -p 6881:6881 -v ${PWD}/rtorrent:/mnt/rtorrent --name ${CONTAINER_NAME} ${TAGNAME}

exec-sh:
	docker exec -it ${CONTAINER_NAME} sh

stop:
	docker stop ${CONTAINER_NAME}


# for deploy with docker compose
docker-build:
	docker compose build

docker-up:
	docker compose up -d

docker-sh:
	docker compose exec ${CONTAINER_NAME} sh

docker-down:
	docker compose down


# for deploy with kubernetes
k3d-cluster-create:
	k3d cluster create ${K3D_CLUSTER_NAME} -v $(PWD)/rtorrent:/mnt/rtorrent-exp@server[0]

k3d-deploy:
	kubectl apply -f deploy

kubectl-sh:
	kubectl exec -it $(shell kubectl get pod -l app=${CONTAINER_NAME} -o jsonpath="{.items[0].metadata.name}") -- sh

kubectl-delete-pod:
	kubectl delete pod $(shell kubectl get pod -l app=${CONTAINER_NAME} -o jsonpath="{.items[0].metadata.name}")

k3d-cluster-delete:
	k3d cluster delete ${K3D_CLUSTER_NAME}
